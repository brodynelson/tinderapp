//
//  User.swift
//  TinderApp
//
//  Created by Brody Nelson on 29/06/16.
//  Copyright © 2016 Brody Nelson. All rights reserved.
//

import Foundation

class User {
    
    var username = ""
    var images : [String : String] = [:]
    
}