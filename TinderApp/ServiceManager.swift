//
//  ServiceManager.swift
//  TinderApp
//
//  Created by Brody Nelson on 28/06/16.
//  Copyright © 2016 Brody Nelson. All rights reserved.
//

import Foundation

//curl

final class ServiceManager {
    var users: [User]? = []
    
    class var sharedManager: ServiceManager {
        struct Static {
            static let instance: ServiceManager = ServiceManager()
        }
        return Static.instance
    }
    
    init() {
        
    }
    
    func getUser() {
        let url = NSURL(string:"http://uifaces.com/api/v1/random")
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
           // print(NSString(data: data!, encoding: NSUTF8StringEncoding))
            
            let jsonResults : AnyObject
            
            do {
                jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                // success ...
                if let user = jsonResults as? [String:AnyObject] {
                    
                    let newUser = User()
                    
                    if let name = user["username"] as? String {
                        newUser.username = name
                    }
                    if let images = user["image_urls"] as? [String : String] {
                        newUser.images = images
                    }
                    
                    self.users?.append(newUser)
                }
                
            } catch {
                // failure
                print("Fetch failed: \((error as NSError).localizedDescription)")
            }
            
        }
        
        task.resume()

    }
    
      
    
}

