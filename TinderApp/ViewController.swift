//
//  ViewController.swift
//  TinderApp
//
//  Created by Brody Nelson on 28/06/16.
//  Copyright © 2016 Brody Nelson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var count = 0
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    
    @IBAction func buttonPressed(sender: AnyObject) {
        if let users = ServiceManager.sharedManager.users {
            let currentUser = users[count]
                label.text = currentUser.username
                if let path = currentUser.images["epic"] {
                        let url = NSURL(string:path)
                    
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                        let data = NSData(contentsOfURL: url!)
                            dispatch_async(dispatch_get_main_queue(), {
                                self.imageView.image = UIImage(data: data!)
                            });
                        }
                    
                }
                
                if count<users.count-1 {
                        count = count + 1
                } else {
                    count = 0
                }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0...10 {
            ServiceManager.sharedManager.getUser()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

